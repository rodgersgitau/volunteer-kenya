const express = require("express");
const graphqlHTTP = require("express-graphql");
const schema = require("./schemas/schema");
const mongoose = require("mongoose");
const cors = require("cors");

require("dotenv").config();

const app = express();
const { DB_USER, DB_PASS } = process.env;
const port = process.env.PORT || 9027;

// mongose
mongoose.connect(
  `mongodb://${DB_USER}:${DB_PASS}@ds219191.mlab.com:19191/volunteer-kenya`
);

mongoose.connection.once("open", () => {
  console.log("Connected to Database");
});

//cors
app.use(cors());

app.use(
  "/graphql",
  graphqlHTTP({
    schema,
    graphiql: true
  })
);

app.listen(port, () => {
  console.log(`=> Server Running on Port ${port}`);
});
