const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const eventSchema = new Schema({
  name: String,
  image: String,
  organiser: String,
  location: String,
  date: Date
});

module.exports = mongoose.model("Event", eventSchema);
