const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const memberSchema = new Schema({
  username: String,
  avatar: String,
  joinDate: Date
});

module.exports = mongoose.model("Member", memberSchema);
