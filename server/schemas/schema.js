const graphql = require("graphql");

//models
const Event = require("../models/event");

const {
  GraphQLObjectType,
  GraphQLString,
  GraphQLSchema,
  GraphQLID,
  GraphQLList,
  GraphQLNonNull
} = graphql;

const GraphQLDate = require("graphql-date");

const EventType = new GraphQLObjectType({
  name: "Event",
  description:
    "Volunteer Events are locally organized meetups for the community",
  fields: () => ({
    id: { type: GraphQLID },
    name: { type: GraphQLString },
    image: { type: GraphQLString },
    organiser: { type: GraphQLString },
    location: { type: GraphQLString },
    date: { type: GraphQLDate }
  })
});

const RootQuery = new GraphQLObjectType({
  name: "RootQueryType",
  fields: {
    event: {
      type: EventType,
      args: { id: { type: GraphQLID } },
      resolve(parent, args) {
        return Event.findById(args.id);
      }
    },
    nextEvent: {
      type: EventType,
      description: "Returns the nearest upcoming event",
      resolve(parent, args) {
        return Event.findOne({}).sort({ date: 1 });
      }
    },
    events: {
      description: "A list of all events",
      type: GraphQLList(EventType),
      resolve(parent, args) {
        return Event.find({}).sort({ date: 1 });
      }
    }
  }
});

const Mutation = new GraphQLObjectType({
  name: "Mutations",
  fields: {
    addEvent: {
      type: EventType,
      description: "Create a new event",
      args: {
        id: { type: GraphQLID },
        name: { type: new GraphQLNonNull(GraphQLString) },
        image: { type: GraphQLString },
        organiser: { type: new GraphQLNonNull(GraphQLString) },
        location: { type: new GraphQLNonNull(GraphQLString) },
        date: { type: GraphQLDate }
      },
      resolve(parent, args) {
        let event = new Event({
          name: args.name,
          image: args.image,
          organiser: args.organiser,
          location: args.location,
          date: args.date
        });

        return event.save();
      }
    }
  }
});

module.exports = new GraphQLSchema({
  query: RootQuery,
  mutation: Mutation
});
