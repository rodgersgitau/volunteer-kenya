import React, { Component } from "react";
import { graphql } from "react-apollo";
import { Image } from "cloudinary-react";
import moment from "moment";

import { getEventsQuery } from "../queries/EventQueries";

class EventList extends Component {
  // fetch events
  // get cloudinary image
  displayEvents() {
    const { REACT_APP_CLOUD_NAME } = process.env;
    const { data } = this.props;

    if (data.loading || !data.events) {
      return <div> Loading Events...</div>;
    } else {
      return data.events.map(event => {
        return (
          <div className="events-item" key={event.id}>
            <div className="events-item-image">
              <Image cloudName={REACT_APP_CLOUD_NAME} publicId={event.image} />
            </div>
            <div className="events-item-heading">
              <h3>{event.name}</h3>
            </div>
            <div className="events-item-content">
              <p>
                <span>By:&nbsp;</span>
                <a href="">{event.organiser}</a>
              </p>
              <p>{event.location}</p>
              <p>{this.formatDate(`${event.date}`)}</p>
            </div>
          </div>
        );
      });
    }
  }

  formatDate(str) {
    return moment(str).format("MMMM Do YYYY, h:mm:ss a");
  }

  render() {
    return <div className="events-list">{this.displayEvents()}</div>;
  }
}

export default graphql(getEventsQuery)(EventList);
