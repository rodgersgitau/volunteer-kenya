import React, { Component } from "react";
import { graphql } from "react-apollo";

import { getNextEventQuery } from "../queries/EventQueries";

class EventTimer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      days: 0,
      hours: 0,
      minutes: 0,
      seconds: 0,
      nextEvent: 0
    };
  }

  componentWillMount() {
    this.getTimeUntil(this.state.nextEvent);
  }

  componentDidMount() {
    setInterval(() => this.getTimeUntil(this.state.nextEvent), 1000);
  }

  leading0(num) {
    return num < 10 ? "0" + num : num;
  }

  getTimeUntil(nextEvent) {
    const { data } = this.props;

    if (!data.loading && data.nextEvent) {
      this.setState({
        nextEvent: data.nextEvent.date
      });
    }

    const time = Date.parse(nextEvent) - Date.parse(new Date());

    if (time < 0) {
      this.setState({ days: 0, hours: 0, minutes: 0, seconds: 0 });
    } else {
      const seconds = Math.floor((time / 1000) % 60);
      const minutes = Math.floor((time / 1000 / 60) % 60);
      const hours = Math.floor((time / (1000 * 60 * 60)) % 24);
      const days = Math.floor(time / (1000 * 60 * 60 * 24));
      this.setState({ days, hours, minutes, seconds });
    }
  }

  render() {
    return (
      <div className="event-timer">
        <div className="event-timer-countdown">
          <div className="countdown-item days">
            <div className="countdown-item-figures">
              <span>{this.leading0(this.state.days)}</span>
            </div>
            <div className="countdown-item-captions">Days</div>
          </div>

          <div className="countdown-item hours">
            <div className="countdown-item-figures">
              <span>{this.leading0(this.state.hours)}</span>
            </div>
            <div className="countdown-item-captions">Hours</div>
          </div>

          <div className="countdown-item minutes">
            <div className="countdown-item-figures">
              <span>{this.leading0(this.state.minutes)}</span>
            </div>
            <div className="countdown-item-captions">Minutes</div>
          </div>

          <div className="countdown-item seconds">
            <div className="countdown-item-figures">
              <span>{this.leading0(this.state.seconds)}</span>
            </div>
            <div className="countdown-item-captions">Seconds</div>
          </div>
        </div>
      </div>
    );
  }
}

export default graphql(getNextEventQuery)(EventTimer);
