import React from "react";

import logo from "../images/logo.png";

const Header = () => (
  <nav className="navbar sticky-top navbar-expand-lg navbar-light">
    <div className="container-fluid padding">
      <a className="navbar-brand" href="/">
        <img src={logo} alt="Volunteer Kenya" />
        <span>Volunteer Kenya</span>
      </a>
      <button
        className="navbar-toggler"
        type="button"
        data-toggle="collapse"
        data-target="#navbarNavAltMarkup"
        aria-controls="navbarNavAltMarkup"
        aria-expanded="false"
        aria-label="Toggle navigation"
      >
        <span className="navbar-toggler-icon" />
      </button>
      <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div className="navbar-nav ml-auto">
          <a className="nav-item nav-link active" href="/">
            Home
          </a>
          <a className="nav-item nav-link" href="/events">
            Events
          </a>
          <a className="nav-item nav-link" href="/past">
            Past Events
          </a>
        </div>
      </div>
    </div>
  </nav>
);

export default Header;
