import React, { Component } from "react";

class Auth extends Component {
  render() {
    return (
      <div className="Auth bg-secondary">
        <input type="checkbox" id="form-switch" />
        <form id="signUpForm">
          <div className="form-group">
            <input
              className="form-control"
              type="email"
              placeholder="Email Address"
            />
          </div>
          <div className="form-group">
            <input
              className="form-control"
              type="text"
              placeholder="Username"
            />
          </div>
          <div className="form-group">
            <input
              className="form-control"
              type="password"
              placeholder="Password"
            />
          </div>
          <div className="form-group">
            <button className="btn btn-block btn-primary" type="submit">
              Sign me up
            </button>
          </div>
          <div className="form-group">
            <label htmlFor="form-switch">Already a Member ? Sign In</label>
          </div>
        </form>

        <form id="signInForm">
          <div className="form-group">
            <input
              className="form-control"
              type="email"
              placeholder="Email Address"
            />
          </div>
          <div className="form-group">
            <input
              className="form-control"
              type="password"
              placeholder="Password"
            />
          </div>
          <div className="form-group">
            <button className="btn btn-block btn-primary" type="submit">
              Let Me In
            </button>
          </div>
          <div className="form-group">
            <label htmlFor="form-switch">New Here ? Sign Up </label>
            <p>
              <a className="text-primary" href="/">
                Forgot Password
              </a>
            </p>
          </div>
        </form>
      </div>
    );
  }
}

export default Auth;
