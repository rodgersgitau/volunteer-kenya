import React, { Component } from "react";
import { graphql } from "react-apollo";

import { DatePicker } from "antd";
import Dropzone from "react-dropzone";
import axios from "axios";

import { addEventQuery } from "../queries/EventQueries";

class AddEvent extends Component {
  constructor() {
    super();

    this.state = {
      name: "",
      image: null,
      location: "",
      organiser: "",
      date: ""
    };
  }

  onDrop = async files => {
    this.setState({ image: files[0] });
    console.log(this.props);
  };

  onChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  handleSubmit = async evt => {
    evt.preventDefault();
    const { name, image, location, organiser, date } = this.state;
    const { REACT_APP_CLOUD_NAME, REACT_APP_UPLOAD_PRESET } = process.env;

    const formData = new FormData();
    formData.append("file", image);
    formData.append("upload_preset", REACT_APP_UPLOAD_PRESET);

    const response = await axios.post(
      `https://api.cloudinary.com/v1_1/${REACT_APP_CLOUD_NAME}/image/upload`,
      formData
    );

    const graphqlResponse = await this.props.mutate({
      variables: {
        name,
        image: response.data.public_id,
        location,
        organiser,
        date
      }
    });

    if (graphqlResponse) {
      this.setState({
        name: "",
        image: null,
        location: "",
        organiser: "",
        date: ""
      });
    }
  };

  render() {
    return (
      <div className="events-new">
        <form id="addEvent" onSubmit={this.handleSubmit}>
          <div className="form-group">
            <div className="img-fluid">
              <Dropzone onDrop={this.onDrop} className="image-drop">
                <p>Drag &amp; Drop Image here or Click to upload</p>
              </Dropzone>
            </div>
          </div>
          <div className="form-group">
            <input
              type="text"
              name="name"
              className="form-control"
              placeholder="Name of Event"
              required
              onChange={this.onChange}
              value={this.state.name}
            />
          </div>
          <div className="form-group">
            <input
              type="text"
              name="organiser"
              className="form-control"
              placeholder="Organiser"
              required
              onChange={this.onChange}
              value={this.state.organiser}
            />
          </div>
          <div className="form-group">
            <input
              type="text"
              name="location"
              className="form-control"
              placeholder="Location"
              required
              onChange={this.onChange}
              value={this.state.location}
            />
          </div>
          <div className="form-group">
            <DatePicker
              name="date"
              className="form-control"
              placeholder="Date / Time"
              required
              onChange={this.onChange}
              value={this.state.date}
            />
          </div>
          <div className="form-group">
            <button type="submit" className="btn btn-block btn-primary">
              Add Event
            </button>
          </div>
        </form>
      </div>
    );
  }
}

export default graphql(addEventQuery)(AddEvent);
