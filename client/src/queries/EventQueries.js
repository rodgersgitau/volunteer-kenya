import { gql } from "apollo-boost";

const getEventsQuery = gql`
  {
    events {
      id
      name
      image
      organiser
      location
      date
    }
  }
`;

const getNextEventQuery = gql`
  {
    nextEvent {
      id
      name
      date
    }
  }
`;

const addEventQuery = gql`
  mutation(
    $name: String!
    $image: String!
    $location: String!
    $organiser: String!
    $date: Date!
  ) {
    addEvent(
      name: $name
      image: $image
      location: $location
      organiser: $organiser
      date: $date
    ) {
      id
      name
    }
  }
`;

export { addEventQuery, getEventsQuery, getNextEventQuery };
