import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import Home from "./Home";
import Events from "./Events";
import PastEvents from "./PastEvents";

const Main = () => (
  <Router>
    <Switch>
      <Route exact path="/" component={Home} />
      <Route path="/events" component={Events} />
      <Route path="/past" component={PastEvents} />
    </Switch>
  </Router>
);

export default Main;
