import React from "react";
import EventList from "../components/EventList";
import AddEvent from "../components/AddEvent";

const Events = () => (
  <div className="container-fluid padding">
    <div className="events">
      <h2 className="section-heading">Upcoming Events</h2>
      <div className="events-grid">
        <EventList />
        <AddEvent />
      </div>
    </div>
  </div>
);

export default Events;
