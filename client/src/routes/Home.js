import React from "react";

import EventTimer from "../components/EventTimer";
import Auth from "../components/Auth";
import backdrop from "../images/backdrop.jpg";

const Home = () => (
  <div className="home">
    <div className="container-fluid padding">
      <div className="home-timer">
        <img
          className="img-responsive"
          alt="A community joining hands"
          src={backdrop}
        />
        <div className="home-timer-overlay">
          <h2 className="subtitle">Next Event</h2>
          <EventTimer />
        </div>
      </div>
      <div className="home-content">
        <div className="home-message">
          <h2>Welcome to the Volunteer Kenya Community</h2>
          <p>
            It's extremely difficult for people to identify volunteering
            activities happening in Kenya because there's no singular platform
            connecting those organizing such events and members of the community
            willing to participate. The Volunteer Kenya is a way to bring
            community members together for such noble causes.
          </p>
          <p>
            There are two categories of users for the platform; members &amp;
            organizers. While you can act as both, we highly discourage
            individuals to act as organizers. Instead, kindly register your
            organization or <i>chamas</i> self-help, WhatsApp groups.
          </p>
        </div>
        <div className="home-form">
          <Auth />
        </div>
      </div>
    </div>
  </div>
);

export default Home;
