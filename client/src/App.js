import React, { Component } from "react";
import ApolloClient from "apollo-boost";
import { ApolloProvider } from "react-apollo";

import "./App.css";
import Header from "./components/Header";
import Main from "./routes/Router";

// apollo client
const client = new ApolloClient({
  uri: "/graphql"
});

class App extends Component {
  render() {
    return (
      <ApolloProvider client={client}>
        <div className="App">
          <Header />
          <section className="section">
            <Main />
          </section>
        </div>
      </ApolloProvider>
    );
  }
}

export default App;
